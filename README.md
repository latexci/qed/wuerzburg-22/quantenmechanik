# Quantenmechanik 

These are the lecture notes for the 'Quantenmechanik', taught in NONE at the University of Bonn.

The [latest version][1] is availabe as a pdf download via GitLab runner.
You can also have a look at the generated [log files][2] or visit the
[gl pages][3] index directly.

[1]: https://latexci.gitlab.io/qed/wuerzburg-22/automatentheorie/2022_QED_Quantenmechanik.pdf
[2]: https://latexci.gitlab.io/qed/wuerzburg-22/automatentheorie/2022_QED_Quantenmechanik.log
[3]: https://latexci.gitlab.io/qed/wuerzburg-22/automatentheorie/
