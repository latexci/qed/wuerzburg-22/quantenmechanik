\section{Grundidee}
In einem newtonschen Modell nehmen wir an,
dass jedes Teilchen $x$ durch seinen Ort und seine Geschwindigkeit
eindeutig berechenbar wird.
Befindet sich in einem Universum genau ein Teilchen, und kennen wir Ort
und Geschwindigkeit, so können wir alles vorherberechnen.

In einem quantenmechanischen Modell ist das nicht mehr so, ein
Quantenteilchen stellen wir uns als Funktion $\psi \colon \mathbb{R}^3 \to  \mathbb{C}$
vor.

Für eine Kugel $K\subset \mathbb{R}^3$ sagt das \vocab{Borne'sche Postulat},
dass
\[
  \mathbb{P}(x\in K) \coloneqq  \int_{K} \abs{\psi } ^2 \dx
\]
Das ist aber natürlich gewissermaßen unsere Interpretation der entsprechenden
Funktion.

Damit das ganze sinnvoll wird, müssen wir folgende Forderungen an $\psi $ stellen:

\section{Formales}

\begin{definition}
  Eine \vocab{Wellenfunktion} ist eine Funktion $\psi \colon \mathbb{R}^3 \to \mathbb{C}$,
  sodass folgende Bedingungen gelten:
  \begin{itemize}
    \item $\psi $ ist glatt, also stetig und überall differenzierbar.
    \item $\psi $ ist \vocab{absolut} integrierbar, also in $L^2(\mathbb{R}^3)$.
    \item $\psi $ ist \vocab{normalisierbar}, d.h.~es gibt ein $k$, sodass
      \[
      \int_{\mathbb{R}^3} k \cdot \abs{\psi } ^2 = 1
      \] 
  \end{itemize}
\end{definition}

\begin{theorem}
  Seien $\psi_1$ und $\psi _2$ Wellenfunktionen.
  Dann ist für $a_1$, $a_2\in \mathbb{C}$ auch
  \[
    a_1\varphi_1 + a_2\psi _2
  \] 
  eine Wellenfunktion.
\end{theorem}
\begin{proof}
  Cauchy-Schwarz.
\end{proof}

\begin{abuse}
  Eigentlich ist der Satz falsch, denn die Nullfunktion ist keine Wellenfunktion.
  Eigentlich sollte man wohl sagen, dass alle Funktionen aus $L^2(\mathbb{R}^3)$
  einen Vektorraum bilden,
  und diejenigen, die nicht Null sind, die Wellenfunktionen sind.
\end{abuse}

\begin{remark}
  Beachte, dass eine glatte Funktion, deren $L^2$-Norm Null ist,
  auch überall null ist, denn sie ist fast überall Null.
\end{remark}

\begin{theorem}
  \label{thm:schrödinger-gleichung}
  Sei $\psi  \coloneqq  \psi (x,t)$.
  Dann ist
  \[
  - i \hbar \frac { \partial \psi  } { \partial t }
  =
  \frac { \hbar^2 } { 2m } \frac { \partial ^2\psi  } { \partial x^2 } + u(x) \psi (x,t)
  \]
  Hierbei ist $u$ das \vocab{Potenzial} des Teilchens $x$ im Universum.
\end{theorem}

\begin{notation}
  Die rechte Seite der \nameref{thm:schrödinger-gleichung} notiert man auch als
  \[
  \hat{H}\psi 
  \]
  und nennt $\hat{H}$ den sogenannten \vocab{Hamilton-Operator}. 
\end{notation}

\begin{remark}
  In dieser Formulierung sind wir davon ausgegangen, dass das Universum
  doch eindimensional ist, sonst wird das ganze komplizierter.
\end{remark}

\subsection{Operatoren}
\begin{definition}
  Seien $\psi $, $\varphi $ Wellenfunktionen.
  Das \vocab{innere} Produkt von $\psi $ und $\varphi $ ist definiert als
  \[
    (\psi ,\varphi ) = \int_{\mathbb{R}^3} \psi ^* \varphi \dx
  \] 
\end{definition}
\begin{definition}
  Sei $\psi $ eine Wellenfunktion, die auf $1$ normiert ist.
  Dann ist
   \[
   \left< x \right> _{\psi }
   \coloneqq
   \int_{\mathbb{R}^3} x \abs{\psi } ^2 \dx
    =
    \int_{\mathbb{R}^3} \psi(x,t) ^{*} x \psi(x,t) \dx
  \]
  der \vocab{Erwartungswert} von $x$ zum Zeitpunkt $t$.
\end{definition}
\begin{abuse}
  Beachte, dass bei $\left< x \right> _{\psi }$ das \enquote{$x$}
  Teil der Notation ist, und nur das $\psi $ Daten enthält,
  von der die rechte Seite abhängt.
\end{abuse}

\begin{definition}
  Der Operator $\hat{x}$ ist definiert durch
  \[
  \hat{x}\psi  \coloneqq  x \mapsto x\cdot \psi (x)
  \] 
  also potentiell besser notiert durch $\hat{x}\psi  = \id \cdot  \psi $.
\end{definition}

\begin{fact}
  Nun können wir auch feststellen, dass
  \[
    (\varphi , \hat{x}\varphi) = \left< x \right> _{\psi }
  \] 
\end{fact}

\begin{example}
  Der \vocab{Impuls} ist definiert als
  \begin{IEEEeqnarray*}{rCl}
 \left< p \right> _{\psi }
&  =&
  m \cdot  \frac { d \left< x \right> _{\psi } } { \dt } \\
& =&
  m \cdot \frac{d}{\dt} \int_{\mathbb{R}^3} x \psi ^* \psi \dx\\
&  =&
  m \cdot  \int_{\mathbb{R}^3} x \frac{d}{dt} \left( \psi ^* \psi  \right) \dx\\
&  =&
  m \cdot \int_{\mathbb{R}^3} x \left( \frac { \partial \psi ^* } { \partial t } \psi + \psi ^* \frac { \partial \psi  } { \partial t }  \right) \dx\\
&  \stackrel{\text{\nameref{thm:schrödinger-gleichung}}}{=} &
  \frac { i \hbar } { 2 } \int_{-\infty}^{\infty} x 
  \left( - \frac { \partial ^2 \psi ^* } { \partial x } \psi + \psi ^* \frac { \partial ^2 \psi  } { \partial x^2 }  \right) \dx \\
& = & 
\frac { i\hbar } { 2 } \int_{-\infty}^{\infty} x \frac { \partial  } { \partial x } 
\left( \psi ^* \frac { \partial \psi  } { \partial x } - \psi \frac { \partial \psi ^* } { \partial x }  \right) \dx \\
& \stackrel{\text{*}}{=}  & 
-\frac { i \hbar } { 2 }  \int_{-\infty}^{\infty} \psi ^* \frac { \partial \psi  } { \partial x } 
- \psi \frac { \partial \psi ^* } { \partial x } \dx \\
& = & -i \hbar \int_{-\infty}^{\infty} \psi ^* \frac { \partial \psi  } { \partial x } \dx \\
& = & \int_{\mathbb{R}^3} \psi ^* (-i \hbar \frac { \partial  } { \partial x } ) \psi \dx \\
& = & (\psi , \hat{p}\psi )
  \end{IEEEeqnarray*}
\end{example}

\begin{definition}
  Der $\hat{p}$ Operator ist definiert als
  \[
  \hat{p} \psi  \coloneqq -i \hbar \frac { \partial  } { \partial x } 
  \] 
\end{definition}

\begin{abuse}
  Im Schritt $*$ haben wir hierbei den Term
   \[
   \left[x \left( \psi ^* \frac { \partial \psi  } { \partial x } - \psi \frac { \partial \psi ^* } { \partial x }  \right) \right]_{-\infty}^{\infty}
  \]
  weggelassen, denn sicherlich gilt doch in der Praxis $x \psi  \to 0$, oder? ODER???
\end{abuse}

\begin{definition}
  Der \vocab{Kinetischen Energie Operator}  ist definiert durch
  \[
  \hat{T} \coloneqq  \frac { \hat{p}^2 } { 2m } 
  \] 
\end{definition}

Nehmen wir für einen Moment mal an, dass
\[
\psi (x,t) = λ(x) T(t)
.\] 
Dann ergibt sich
\[
\psi (x,t) = \chi_n (x) \exp(-\frac{1}{\hbar} E_n t)
\]
wobei $\hat{H}\chi_n = E_n\chi_n$ und $E_n$ eine reelle Zahl ist.

\begin{theorem}
  Jede Wellen-Funktion kann geschrieben werden als
  \[
  \psi (x,t) = \sum_{n=1}^{\infty} a_n \psi _n (x,t)
  \]
  wobei die $\psi _n$ die Eigenfunktionen von $\hat{H}$ sind.
\end{theorem}
\begin{proof}
  Sturm-Liouville Theorie zu partiellen Differentialgleichungen.
\end{proof}

\begin{theorem}[Born's Regel II]
  Es gelten die folgenden Fakten in unserer physikalischen Theorie
  \begin{itemize}
    \item Sei $\hat{O}$ ein Operator. $\hat{O}$ ist immer hermitisch.
    \item Wir messen $\hat{O}\psi $ und erhalten eine reelle Zahl.
      Dann können wir nur die Eigenwerte von $\hat{O}$ errhalten.
    \item 
      Schreiben wir wieder $\psi = \sum a_n \psi _n$ mit Eigenwertsfunktionen
      $\varphi _n$ von $\hat{O}$, diesmal
      mit $\sum \abs{a_i} ^2 = 1$.
      Dann ist
      \[
        \mathbb{P}(\hat{O}\psi  = λ_i) = \abs{a_i} ^2 
      \] 
    \item Ich messe $λ_i$ zum Zeitpunkt  $t_0$.
      Dann wird $\psi $ zu $\psi _i$.
  \end{itemize}
\end{theorem}

\begin{fact}[Lineare Algebra]
  Die Eigenwerte einer hermiteschen Matrix sind alle reell.
  Das motiviert Teile der 2. Bourn'schen Regel.
\end{fact}

\begin{remark}
  Mit \enquote{messen} meinen wir eine quantenmechanische Messung,
  also eigentlich schwarze Magie, weil nicht so klar ist, was wir eigentlich tun.
\end{remark}

\begin{theorem}
  Die Aussage
  \[
  \left< O \right> _{\psi } = (\psi , \hat{O}\psi )
  \] 
  aus Born's Regel herleiten.
\end{theorem}

\begin{definition}
  Seien $\hat{A}$ und $\hat{B}$ Operatoren.
  Notiere ihren \vocab{Kommutator}  als
  \[
    [\hat{A}, \hat{B}] = \hat{A}\hat{B} - \hat{B}\hat{A}
  \] 
\end{definition}

\begin{lemma}
  $\hat{A}$, $\hat{B}$ sind genau dann gleichzeitig diagonalisierbar,
  wenn $[\hat{A},\hat{B}] = 0$.
\end{lemma}
\begin{proof}
  Lineare Algebra.
  Die Richtung \enquote{$\implies$} ist trivial, die andere fast trivial.
\end{proof}

\begin{definition}
  Sei $\hat{A}$ ein Operator.
  Dann ist
  \[
    (\Delta_{\psi } \hat{A})^2 = \left< \left(\hat{A} - \left< \hat{A} \right> _{\psi }\right)^2 \right> _{\psi }
  .\] 
  die \vocab{Standardabweichung} von $\hat{A}$. 
\end{definition}

\begin{lemma}
  Es ist $(\Delta_{\psi } \hat{A})^2 \geq 0$, mit Gleichheit
  genau dann, wenn $\psi $ eine Eigenwertfunktion von $\hat{A}$ ist.
\end{lemma}
\begin{proof}
  Cauchy-Schwarz.
\end{proof}

\begin{theorem}[Verallgemeinerte Umschärferelation]
  Seien $\hat{A}$ und  $\hat{B}$ Operatoren.
  \[
    (\Delta_{\psi } \hat{A}) (\Delta_{\psi }\hat{B}) \geq \frac{1}{2} \abs{(\psi , [\hat{A},\hat{B}]\psi } 
  \] 
\end{theorem}
\begin{proof}
  Es ist $A' = \left< A^2 \right> _\psi  - \left< A \right> ^2_\psi $ und anlog für $B$.
  Dam ist
   \begin{IEEEeqnarray*}{rCl}
     \left( \Delta_{\psi }A \right) ^2(\Delta_{\psi } B)^2
     & = &
     (A' \psi , A'\psi )(B'\psi , B'\psi ) \\
     & \geq  &
     \abs{(A' \psi , B'\psi )}^2 \\
     & \stackrel{\text{*}}{=} & \abs{(\psi , A'B'\psi )} ^2
  \end{IEEEeqnarray*}
  Als Nebenrechnung haben wir hierbei
  \begin{IEEEeqnarray*}{rCl}
    (\Delta_{\psi }A)^2 & = &
    \left< \left( A - \left< A \right> _{\psi } \right) ^2 \right> _{\psi } \\
                        & = &
                        \left< \hat{A}^2 - 2 \hat{A}\left< \hat{A} \right> _{\psi } + \left< \hat{A} \right> ^2_{\psi } \right> _{\psi } \\
                        & = & 
                        \left< \hat{A}^2 \right> _{\psi } - 2 \left< \hat{A} \right> ^2_{\psi } + \left< \hat{A} \right> ^2_{\psi }\\
                        &=& \left< \hat{A}^2 \right> _{\psi } - \left< \hat{A} \right> ^2_{\psi }
  \end{IEEEeqnarray*}
  Außerdem haben wir geschummelt, weil $*$ nur für hermitesche Operatoren gilt,
  aber das sind ja für uns alle.
\end{proof}

\begin{definition}
  Der \vocab{Anti-Kommutator} von zwei Operatoren
  $\hat{A}$ und $\hat{B}$ ist definiert als:
   \[
    \left\{\hat{A}, \hat{B}\right\} \coloneqq  \hat{A}\hat{B} + \hat{B}\hat{A}
  \]
\end{definition}

\begin{lemma}
  Es ist
  \[
    A'B' = \frac{1}{2}\left( [\hat{A},\hat{B}] + \left\{\hat{A}, \hat{B}\right\}  \right) 
  .\]
  sowie
  \[
    \left< [A',B'] \right> _{\psi } = \ldots = (A'\psi , B'\psi ) - (B'\psi, A'\psi ) \in  i\mathbb{R}
  \]
  und
  \[
  \left< \left\{A',B'\right\}  \right> _{\psi } = (\psi , (A'B' + B'A')\psi ) = \ldots \in \mathbb{R}
  \]
  und
  \[
    \abs{\psi , A'B'\psi } ^2 = \frac{1}{4} \left( \abs{\psi , [A',B']\psi }_{\psi } ^2 + 
    \abs{\left< \psi , \left\{A',B'\right\} \psi  \right> _{\psi }}^2 \right) 
  \] 
\end{lemma}

\begin{theorem}[Heisenbergsche Unschärferelation]
  Es ist
  \[
      [\hat{x}, \hat{p}] = i \hbar I
  \]
  und damit
  \[
    (\Delta_{\psi } \hat{x}) ( \Delta_{\psi } \hat{p}) \geq \frac { \hbar } { 2 } 
  \] 
\end{theorem}

Was noch kommen würde: Satz von Ehrenfest.
